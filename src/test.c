//
// Created by etoxto on 1/26/22.
//
#define _DEFAULT_SOURCE
#include "test.h"

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"


#define HEAP_SIZE 10000
#define MALLOC_SIZE 1000

static void* heap;
static struct block_header* start_heap;
void debug(const char* fmt, ... );

void test_heap_init(){
    heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        err ("err: Не получилось выделить кучу\n");
    } else {
        debug_heap (stderr, heap);
    }
    start_heap = (struct block_header*) heap;
}

void test_mem_allocate(){
    debug("--- Тест 1: выделение блока ---\n");
    void* test1 = _malloc(MALLOC_SIZE);
    if (test1 == NULL) {
        err ("err: Ошибка при выделении памяти\n");
    }
    debug_heap (stderr, heap);
    if (start_heap->capacity.bytes != MALLOC_SIZE || start_heap->is_free) {
        err ("err: Ошибка при выделении памяти\n");
    }
    debug ("Память успешно выделена\n");
}

void test_free_one_block(){
    debug("--- Тест 2: освобождение блока ---\n");
    void* test2= _malloc(MALLOC_SIZE);
    _malloc(MALLOC_SIZE);
    _free(test2);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) test2)-offsetof(struct block_header, contents)))->is_free) {
        err("err: Ошибка при очистке памяти\n");
    }
    debug ("Память успешно очищена");
}

void test_free_two_blocks(){
    debug("--- ест 3: выделение и освобождение 2 блоков ---\n");
    void* first = _malloc(MALLOC_SIZE);
    void* second = _malloc(MALLOC_SIZE);
    _free(first);
    _free(second);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) first)-offsetof(struct block_header, contents)))->is_free ||
        !((struct block_header*) (((uint8_t*) second)-offsetof(struct block_header, contents)))->is_free) {
        err("err: Ошибка при очистке памяти: блок не помечен как свободный\n");
    }
    debug ("Память успешно очищена\n");
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
            struct block_header, contents));
}

void new_region_extends_old_test() {
    debug("\n --- Тест 4: расширение кучи с выделением региона последовательно ---\n");
    const size_t SZ = 2 * HEAP_SIZE;
    void* mem = _malloc(SZ);
    debug_heap(stderr, heap);
    if (mem == NULL)
        err("Ошибка при выделении памяти: Null pointer.");
    struct block_header *last = block_get_header(mem);
    if(last->capacity.bytes != SZ)
        err("Ошибка при выделении памяти: размер выделенной памяти не соответствует запрошенному.");
    if(last->is_free)
        err("Ошибка при выделении памяти: блок не помечен как занятый.");
    _free(mem);
    if(!last->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);

}

void new_region_another_place_test(){
    debug("\n --- Тест 5: расширение кучи с выделением региона в другом месте ---\n");
    const size_t SZ = HEAP_SIZE * 4;
    struct block_header* iter = start_heap;
    if(!iter) {
        err("Ошибка: тесты должны быть запущены последовательно.");
        return;
    }
    struct block_header* last = iter;
    while(iter) {
        last = iter;
        iter = iter->next;
    }
    void* addr = (uint8_t*) last + size_from_capacity(last->capacity).bytes;
    addr = map_pages(addr, 1000,  MAP_FIXED_NOREPLACE);
    debug("\nАдрес, по которому память стала недоступна: %p.\n", addr);
    void* mem = _malloc(SZ);
    struct block_header* new = block_get_header(mem);
    if (mem == NULL)
        err("Ошибка при выделении памяти: Null pointer.");
    if(!new)
        err("Ошибка при выделении памяти: блок не встроен в кучу.");
    if(new->capacity.bytes != SZ)
        err("Ошибка при выделении памяти: размер выделенной памяти не соответствует запрошенному.");
    if(new->is_free)
        err("Ошибка при выделении памяти: блок не помечен как занятый.");
    debug_heap(stderr, heap);
    _free(mem);
    if(!new->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);
}
