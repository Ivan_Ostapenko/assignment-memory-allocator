//
// Created by etoxto on 1/26/22.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

void test_heap_init();
void test_mem_allocate();
void test_free_one_block();
void test_free_two_blocks();
void new_region_extends_old_test();
void new_region_another_place_test();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
