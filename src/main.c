//
// Created by etoxro on 1/26/22.
//

#include <stdio.h>

#include "test.h"

int main(){
    test_heap_init();
    test_mem_allocate();
    test_free_one_block();
    test_free_two_blocks();
    new_region_extends_old_test();
    new_region_another_place_test();

    return 0;
}
